<?php

namespace Drupal\Teedee_Chatbot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provide settings page for adding CSS/JS before the end of body tag.
 */
class FooterForm extends ConfigFormBase {

  /**
   * Implements FormBuilder::getFormId.
   */
  public function getFormId() {
    return 'hfs_footer_settings';
  }

  /**
   * Implements ConfigFormBase::getEditableConfigNames.
   */
  protected function getEditableConfigNames() {
    return ['Teedee_Chatbot.footer.settings'];
  }

  /**
   * Implements FormBuilder::buildForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $footer_section = $this->config('Teedee_Chatbot.footer.settings')->get();

    $form['hfs_footer'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Add Scripts and Styles in Footer'),
      '#description' => $this->t('All the defined scripts and styles in this section would be added just before closing the <strong>body</strong> tag.'),
    ];

    

    $form['hfs_footer']['scripts'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Bot  Script'),
      '#default_value' => isset($footer_section['scripts']) ? $footer_section['scripts'] : '',
      '#description'   => $this->t('<p>Add your chatbot script above</p>'),
      '#rows'          => 10,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements FormBuilder::submitForm().
   *
   * Serialize the user's settings and save it to the Drupal's config Table.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configFactory()
      ->getEditable('Teedee_Chatbot.footer.settings')
      ->set('styles', $values['styles'])
      ->set('scripts', $values['scripts'])
      ->save();

    drupal_set_message($this->t('Your Settings have been saved.'), 'status');
  }

}
