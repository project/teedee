<<<<<<< HEAD
INTRODUCTION
------------

Teedee Chatbot  module for Drupal 8.x.
This module allows you to add chatbot scripts in different region of the page
from the front-end. You don't need to open any file for this purpose.

This module adds the Teedee Chatbot script on all over the site , allowing you add a chatbot to your own website. There are 3
setting pages. These setting pages are allow you add the scripts
in the desired region.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions

   - Access Header Footer Scripts

     Users in roles with "Add Scripts all over the site" permission will
     add / remove the styles and scripts from any position.

 * Add styles and scripts in settings on the Administer ->
   Configuration -> Development -> Header Footer Scripts Page.

MAINTAINERS
-----------

Current maintainers:
 * Vaibhav Dubey (Vaibhav-Dubey) - https://www.drupal.org/user/3647244
 * 

This project has been sponsored by:
 * Botonomic Automation LLP
  A Startup that specialise in building powerful chatbots and provide various AI and ML based solutions for all your business needs.
=======
>>>>>>> 29ce071ff8b47076a15b7d95b2005d1931badc01
